import numpy as np


def mag_cal(Mag_conv):
    # Correction factor
    h1 = np.array([[-1273.2875], [-1094.0125], [0]])
    h2 = np.array([[3820.4625], [3256.5125], [0]])
    h3 = np.array([[-2252.95], [-2019.4], [0]])

    # Calibration matrices
    CM_mm01 = np.array(
        [[36945, 554, -1015], [1386, -33912, -9491], [-1598, -302, 34636]]
    )
    CM_mm02 = np.array([[-36971, -75, 505], [297, 34342, -1557], [-3988, 292, 32701]])
    CM_mm03 = np.array([[10055, 6, 193], [-4, 9800, -140], [-189, 134, 10255]])

    CO_mm01 = np.array([[2.5051], [2.5712], [2.4932]]) + np.matmul(
        np.linalg.inv(CM_mm01), h1
    )
    CO_mm02 = np.array([[2.624], [2.5702], [2.4908]]) + np.matmul(
        np.linalg.inv(CM_mm02), h2
    )
    CO_mm03 = np.array([[0.05269], [-0.02343], [-0.10125]]) + np.matmul(
        np.linalg.inv(CM_mm03), h3
    )

    # Transforming data to T

    MGM1_conv = np.column_stack(
        (Mag_conv[:, 0], Mag_conv[:, 1], Mag_conv[:, 2])
    ).transpose()
    MGM2_conv = np.column_stack(
        (Mag_conv[:, 3], Mag_conv[:, 4], Mag_conv[:, 5])
    ).transpose()
    MGM3_conv = np.column_stack(
        (Mag_conv[:, 6], Mag_conv[:, 7], Mag_conv[:, 8])
    ).transpose()

    MGM1 = np.matmul(CM_mm01, (MGM1_conv - CO_mm01)).transpose()
    MGM2 = np.matmul(CM_mm02, (MGM2_conv - CO_mm02)).transpose()
    MGM3 = np.matmul(CM_mm03, (MGM3_conv - CO_mm03)).transpose()

    return MGM1, MGM2, MGM3
