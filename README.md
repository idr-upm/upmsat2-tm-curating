# UPMSat-2 TM Curating

This repository contains the code necessary to combine and filter the telemetry
(TM) received by the Ground Station (GS) of IDR/UPM and the Satnogs network. It
also computes the position and velocity of th UPMSat-2 at the reception time of
each TM entry and the value of the Earth's magnetic field in such a position.

At this moment, the code only retrieves and calibrates the magnetometers
measurements.


### Prerequisites

Dependencies:

* pandas
* numpy
* matplotlib
* astropy
* sgp4
* ppigrf
