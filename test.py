import unittest
from satellite import Satellite
from sgp4.api import Satrec
import numpy as np


class TestSatellitePropagation(unittest.TestCase):
    # Test implementation of Satellite class
    def setUp(self):
        self.upmsat = Satellite()
        self.jd = 2459458
        self.fr = 0.5
        self.date_jd = self.jd + self.fr

    def test_propagation(self):
        # Test propagation
        time, r, v = self.upmsat.propagate(self.jd + self.fr)

        s = self.upmsat._test.TLE_lines[0]
        t = self.upmsat._test.TLE_lines[1]
        satellite = Satrec.twoline2rv(s, t)

        _, r_ref, v_ref = satellite.sgp4(self.jd, self.fr)

        self.assertAlmostEqual(time.to_value("jd"), self.date_jd, places=6)
        self.assertListAlmostEqual(r, r_ref)
        self.assertListAlmostEqual(v, v_ref)

    def assertListAlmostEqual(self, list1, list2, places=7):
        self.assertEqual(len(list1), len(list2))
        for elem1, elem2 in zip(list1, list2):
            self.assertAlmostEqual(elem1, elem2, places=places)

    def test_time(self):
        # Test if the choosen TLE is the correct one (closer in time)
        _ = self.upmsat.propagate(self.jd + self.fr)
        TLE_index = np.where(self.upmsat.TLE_list == self.upmsat._test)[0][0]

        assert self.date_jd > self.upmsat._test.epoch.jd
        assert self.date_jd < self.upmsat.TLE_list[TLE_index + 1].epoch.jd


if __name__ == "__main__":
    unittest.main()
