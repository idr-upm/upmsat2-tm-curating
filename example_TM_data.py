from satellite import Satellite
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

upmsat = Satellite()

upmsat.generate_csv(save=False)

B = upmsat.orbital_df[['Bx', 'By', 'Bz']].values
r = upmsat.orbital_df[['x_ECEF', 'y_ECEF', 'z_ECEF']].values
x, y, z = r.T
Bx, By, Bz = B.T

# Create a 3D subplot
ax_vf = plt.figure().add_subplot(projection="3d")

# Add a quiver plot for the magnetic field
ax_vf.quiver(x, y, z, Bx, By, Bz, length=1000, normalize=True, color="r")

# Create a sphere
u = np.linspace(0, 2 * np.pi, 100)
v = np.linspace(0, np.pi, 100)
x_sphere = 6378 * np.outer(np.cos(u), np.sin(v))
y_sphere = 6378 * np.outer(np.sin(u), np.sin(v))
z_sphere = 6378 * np.outer(np.ones(np.size(u)), np.cos(v))

# Plot the sphere
ax_vf.plot_surface(x_sphere, y_sphere, z_sphere, color="b", alpha=0.3)

ax_vf.set_box_aspect((np.ptp(x_sphere), np.ptp(y_sphere), np.ptp(z_sphere)))

plt.show()
