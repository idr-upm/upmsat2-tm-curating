import numpy as np
import pandas as pd
import os
from astropy.time import Time, TimeDelta
from sgp4.api import Satrec

import US2_conversion as US2_con
import US2_calibration as US2_cal


def _propagate(TLE_sat, starting_date, propagation_time):
    """
    This function propagates satellite using SPG4.

    Returns raw x, y, z Cartesian coordinates in a 'True Equator Mean Equinox'
    (TEME) reference frame that is centered on the Earth but does not rotate
    with it — an 'Earth centered inertial' (ECI) reference frame.

    Parameters
    ----------
    TLE_sat: TLE class
        TLE object with the orbit info
    starting_date: astropy time class
        Start time for propagation (astropy Time class)
    propagation_time: float
        Propagation period [days]

    Returns
    -------
    t :  ndarray
        Final time as astropy.time.Time class
    r :  ndarray
        Position of satellite in TEME frame [km]
    v :  ndarray
        Velocity of satellite in TEME frame [km/s]
    """

    t0 = starting_date.jd
    tf = t0 + propagation_time

    jd = int(np.floor(tf))
    fr = tf - jd

    sat = Satrec.twoline2rv(*TLE_sat.TLE_lines)

    _, r, v = sat.sgp4(jd, fr)

    r, v = np.array(r), np.array(v)

    t = starting_date + TimeDelta(propagation_time, format="jd")

    return t, r, v


class TLE:
    """
    Class representing a Two-Line Element (TLE) set for a satellite.

    Parameters
    ----------
    name : str
        The name of the satellite
    line_1 : str
        The first line of the TLE set
    line_2 : str
        The second line of the TLE set

    Attributes
    ----------
    name : str
        The name of the satellite
    line_1 : str
        The first line of the TLE set
    line_2 : str
        The second line of the TLE set

    Properties
    ----------
    TLE_lines : tuple of str
        A tuple containing the TLE lines (line_1, line_2)
    id : str
        The NORAD Catalog Number (id) of the satellite
    epoch : astropy.time.Time
        The epoch time in UTC when the TLE set was valid

    Example
    -------
    >>> line1 = "1 25544U 98067A   20322.44367824  .00000852  00000-0  28403-4 0  8652"
    >>> line2 = "2 25544  51.6450 247.4627 0006704 130.5360 325.0288 15.49447914420205"
    >>> tle = TLE("ISS (ZARYA)", line1, line2)
    >>> print(tle.id)
    '25544'
    >>> print(tle.epoch)
    2020-11-17 10:37:03.999
    """

    def __init__(self, name, line_1, line_2):
        """
        Initialize a TLE object.

        Parameters
        ----------
        name : str
            The name of the satellite.
        line_1 : str
            The first line of the TLE set.
        line_2 : str
            The second line of the TLE set.
        """

        self.name = name
        self.line_1 = line_1
        self.line_2 = line_2

    @property
    def TLE_lines(self):
        """
        Property: Get the TLE lines as a tuple (line_1, line_2).

        Returns
        -------
        tuple of str
            A tuple containing the TLE lines (line_1, line_2)
        """
        return (self.line_1, self.line_2)

    @property
    def id(self):
        """
        Property: Get the NORAD Catalog Number (id) of the satellite.

        Returns
        -------
        str
            The NORAD Catalog Number (id) of the satellite
        """
        return self.line_2[2:7]

    @property
    def epoch(self):
        """
        Property: Get the epoch time in UTC when the TLE set was valid.

        Returns
        -------
        epoch_time : astropy.time.Time class
            The epoch time in UTC when the TLE set was valid
        """
        epoch_year = int(self.line_1[18:20])
        epoch_day = float(self.line_1[20:32])

        epoch_time = Time(
            str(2451544.50000 + epoch_year * 365.25 + epoch_day - 1),
            format="jd",
            scale="utc",
        )
        return epoch_time


def read_TLE_file(file_name):
    """
    Read TLEs from a file and return a list of TLE objects.

    Parameters
    ----------
    file_name : str
        The filename of the TLE data file

    Returns
    -------
    TLE_list : list of TLE
        A list of TLE objects representing the TLE sets from the file
    """

    with open(file_name) as f:
        TLE_list = []
        second = False
        line1 = None
        for line in f:
            if second:
                TLE_list.append(TLE("Sat", line1, line))
                second = False
            else:
                line1 = line
                second = True

    return TLE_list


def read_files_names(folder):
    """
    Read and return a list of filenames ending with '.csv' in the specified
    folder.

    Parameters
    ----------
    folder : str
        The path to the folder containing CSV files

    Returns
    -------
    files_names : list of str
        A list of filenames ending with '.csv' in the specified folder
    """

    files_names = []
    for file in os.listdir(folder):
        if file.endswith(".csv"):
            files_names.append(file)

    return files_names


def get_files(folder):
    """
    Read and concatenate CSV files from a folder into a single DataFrame.

    Parameters
    ----------
    folder : str
        The path to the folder containing CSV files
    parse : str
        The column name to parse as dates in the CSV files

    Returns
    -------
    df : pandas DataFrame
        DataFrame containing data from all the CSV files in the folder.
    """

    files_names = read_files_names(folder)
    df = pd.read_csv(folder + "/" + files_names[0], sep=";")

    for file in files_names[1:-1]:
        data = pd.read_csv(folder + "/" + file, sep=";")
        df = pd.concat([df, data])

    return df


def cartesian_to_spherical(x, y, z):
    """
    Convert Cartesian coordinates to spherical coordinates.

    Parameters
    ----------
    x : ndarray (n)
        The x-coordinate(s) of the point(s) in Cartesian space [km]
    y : ndarray (n)
        The y-coordinate(s) of the point(s) in Cartesian space [km]
    z : ndarray (n)
        The z-coordinate(s) of the point(s) in Cartesian space [km]

    Returns
    -------
    rho : ndarray (n)
        The radial distance from the origin to the points [km]
    theta_deg : ndarray (n)
        The colatitude [deg], measured from the positive z-axis
    phi_deg : ndarray (n)
        The longitude [deg], measured from the positive x-axis in the xy-plane
    """

    rho = np.sqrt(x**2 + y**2 + z**2)
    phi = np.arctan2(y, x)
    theta = np.arccos(z / rho)

    theta_deg = np.degrees(theta)

    phi_deg = np.degrees(phi)

    return rho, theta_deg, phi_deg


def Rc2s(theta, phi):
    """
    Coordinate transformation matrix from the cartesian frame to the the
    spherical frame based on the spherical coordinates.

    Theta and phi are the spherical coordiantes of the spherical frame's origin

    Parameters
    ----------
    theta : float
        The colatitude [deg], measured from the positive z-axis
    phi : float
        The longitude [deg], measured from the positive x-axis in the xy-plane

    Returns
    -------
    R : ndarray (3, 3)
        Coordinate transformation matrix
    """

    theta_rad = theta * np.pi / 180
    phi_rad = phi * np.pi / 180

    R = np.array(
        [
            [
                np.sin(theta_rad) * np.cos(phi_rad),
                np.sin(theta_rad) * np.sin(phi_rad),
                np.cos(theta_rad),
            ],
            [
                np.cos(theta_rad) * np.cos(phi_rad),
                np.cos(theta_rad) * np.sin(phi_rad),
                -np.sin(theta_rad),
            ],
            [-np.sin(phi_rad), np.cos(phi_rad), 0],
        ]
    )

    return R


def curate_TM_data(GS_file="HK_TM_31oct_00-48.csv"):
    """
    Retrieves the TM from the GS and Satnogs, combining and cleaning it. Then,
    the telemetry from the magnetometers is retrived and calibrated.

    Parameters
    ----------
    GS_file : str
        The filename of the GS (Ground Station) data CSV file

    Returns
    -------
    calibrated_df: pandas DataFrame
        DataFrame containing the calibrated TM data of the magnetometers
    """

    df1 = get_files("Satnogs/Historico_Satnogs")
    df2 = pd.read_csv("GS/Historico_GS/" + GS_file, sep=";", on_bad_lines="skip")

    # Data curating Satnogs
    df1.drop(
        columns=["Unnamed: 85", "Source", "Operating_Mode", "RESERVED"], inplace=True
    )  # Delte last columns
    df1.drop(
        columns=["Battery_Warning", "DAS_P3V", "DAS_P5V", "DAS_P15V", "DAS_N15V"],
        inplace=True,
    )
    df1.drop(
        columns=[
            "PDU_P3V3",
            "PDU_P5V",
            "MGM1_P5V",
            "MGM2_P5V",
            "MGM3_P15V",
            "MGM3_N15V",
            "MGT_X_VBUS",
        ],
        inplace=True,
    )
    df1.drop(
        columns=[
            "RW_VBUS",
            "MTS_VBUS",
            "BOOM1_VBUS",
            "BOOM1_VBUS",
            "BOOM2_VBUS",
            "TTC_STAT",
            "SMA_SB01",
            "SMA_SB02",
        ],
        inplace=True,
    )
    df1.drop(columns=["TEMP_A_P5V", "TEMP_B_P5V", "MODEM_VBUS", "RW_P5V"], inplace=True)
    df1.drop(
        columns=["TP1_TM", "TP2_TM", "TP3_TM", "TP4_TM", "TP5_TM", "TP6_TM"],
        inplace=True,
    )

    df1.drop_duplicates(subset=["Misison_Clock"], keep="last", inplace=True)
    df1.drop_duplicates(subset=["Received"], keep="last", inplace=True)

    # Formatting of reception date
    df1["Received"] = pd.to_datetime(df1["Received"], format="mixed")
    df1["Received"] = df1["Received"].dt.tz_localize("UTC")
    df1["JD"] = pd.DatetimeIndex(df1["Received"]).to_julian_date()

    df1.dropna(inplace=True)

    # GS data curating
    # Remove useless columns
    df2.drop(
        columns=["Operating_Mode", "BATT_VBUS_TM"], inplace=True
    )
    df2.drop(
        columns=["Battery_Warning", "DAS_P3V", "DAS_P5V", "DAS_P15V", "DAS_N15V"],
        inplace=True,
    )
    df2.drop(
        columns=[
            "PDU_P3V3",
            "PDU_P5V",
            "MGM1_P5V",
            "MGM2_P5V",
            "MGM3_P15V",
            "MGM3_N15V",
            "MGT_X_VBUS",
        ],
        inplace=True,
    )
    df2.drop(
        columns=[
            "RW_VBUS",
            "MTS_VBUS",
            "BOOM1_VBUS",
            "BOOM1_VBUS",
            "BOOM2_VBUS",
            "TTC_STAT",
            "SMA_SB01",
            "SMA_SB02",
        ],
        inplace=True,
    )
    df2.drop(columns=["TEMP_A_P5V", "TEMP_B_P5V", "MODEM_VBUS", "RW_P5V"], inplace=True)
    df2.drop(
        columns=[
            "MTS_P1TTS1_TM",
            "MTS_P1TTS2_TM",
            "MTS_P1TTS3_TM",
            "MTS_P1TTS4_TM",
            "MTS_P1TTS5_TM",
            "MTS_P1TTS6_TM",
        ],
        inplace=True,
    )
    df2.dropna(axis=1, inplace=True)

    df2.drop_duplicates(subset=["Misison_Clock"], keep="last", inplace=True)
    df2.drop_duplicates(subset=["Received"], keep="last", inplace=True)

    # Formatting of reception date
    df2["Received"] = pd.to_datetime(df2["Received"], format="%Y-%m-%d %H:%M:%S")
    df2["Received"] = df2["Received"].dt.tz_localize("Europe/Madrid")
    df2["Received"] = df2["Received"].dt.tz_convert("UTC")
    df2["JD"] = pd.DatetimeIndex(df2["Received"]).to_julian_date()

    ######### Join both dataframes

    df1.set_index("Received", inplace=True)
    df2.set_index("Received", inplace=True)

    df = pd.concat([df2, df1], verify_integrity=True)

    df.sort_index(ascending=True, inplace=True)

    # Conversion de AC units to Physical ones
    Raw_mag = df[
        [
            "MGM1_X_TM",
            "MGM1_Y_TM",
            "MGM1_Z_TM",
            "MGM2_X_TM",
            "MGM2_Y_TM",
            "MGM2_Z_TM",
            "MGM3_X_TM",
            "MGM3_Y_TM",
            "MGM3_Z_TM",
        ]
    ].values.copy()

    Con_mag = US2_con.mag_conversion(Raw_mag)

    # Calibration

    MGM1, MGM2, MGM3 = US2_cal.mag_cal(Con_mag)

    columns_name = [
        "JD",
        "mgm_1_x",
        "mgm_1_y",
        "mgm_1_z",
        "mgm_2_x",
        "mgm_2_y",
        "mgm_2_z",
        "mgm_3_x",
        "mgm_3_y",
        "mgm_3_z",
    ]
    JD = df["JD"].values.reshape(-1, 1)

    calibrated_df = pd.DataFrame(
        data=np.concatenate((JD, MGM1, MGM2, MGM3), axis=1),
        columns=columns_name,
        index=df.index,
    )

    return calibrated_df
