import numpy as np
import pandas as pd
import os

from tools import (
    read_TLE_file,
    _propagate,
    cartesian_to_spherical,
    Rc2s,
    curate_TM_data,
)

from astropy import units as u
from astropy.coordinates import (
    TEME,
    CartesianRepresentation,
    CartesianDifferential,
    ITRS,
)

import ppigrf


class Satellite:
    """
    A global class to read, combine and clean the TM of the UPMSat-2 TM from the
    Ground Segment (GS) and Satnogs. It also computes the position and velocity
    of the satellite (based on the TLEs) for the time registered in each TM
    entry and the value of the Earth's Magnetic Field in such position and time
    using the IGRF model. These three components are referred here as "orbital
    data". TM and orbital data is finally combined in a single dataframe and
    stored in a csv file.

    Parameters
    ----------
    **kwargs : dict
        Keyword arguments for initializing the satellite. Default values are
        provided in `_defaults`

    Attributes
    ----------
    TLE_filename : str
        The filename of the Two-Line Element (TLE) data file
    GS_TM_filename : str
        The filename of GS TM data file
    folder : str
        Relative path to folder where the final csv shall be stored
    filename : str
        Name of the final csv file
    TLE_list : list of TLE objects
        List with all the historic TLEs
    TLE_time_utc : list of floats
        List with the epoch of each TLE expressed in jd (UTC)
    TM_all_df : pandas DataFrame
        Dataframe with TM and orbital data
    TM_df : pandas DataFrame
        Dataframe with TM
    orbital_df : pandas DataFrame
        Dataframe with orbital data


    Methods
    -------
    load_TLE()
        Loads the historic set of TLEs from the UPMSat-2
    propagate(t)
        Propagate the satellite's state to a specific time
    propagate_array(times)
        Propagate the satellite's state for an array of times
    generate_orbital_data()
        Retrieves position, velocity and value of the Earth's magnetic field
        for a given set of times
    generate_csv()
        Generates and saves the final csv file combining TM and orbital data
    save_data()
        Saves the final csv file
    """

    _defaults = {
        "TLE_filename": "upmsat2_historical_TLE.txt",
        "GS_TM_filename": "HK_TM_31oct_00-48.csv",
        "folder": None,
        "filename": "TM_mgm_upmsat2_clean",
    }

    def __init__(self, **kwargs):
        """
        Initialize the Satellite object.

        Parameters
        ----------
        **kwargs : dict
            Keyword arguments for initializing the satellite. Default values are
            provided in `_defaults`
        """

        self.__dict__.update(self._defaults)
        self.__dict__.update(kwargs)
        self.load_TLE()

    def load_TLE(self):
        """
        Loads TLE from files, compute TLE times and removes duplicates.
        """

        TLE_list_original = read_TLE_file("TLEs/" + self.TLE_filename)
        dates_list = [TLE.epoch for TLE in TLE_list_original]

        delta_t_list = [
            86400 * (dates_list[i + 1] - dates_list[i]).jd
            for i in range(len(dates_list) - 1)
        ]

        delta_t = np.array(delta_t_list)

        mask = np.concatenate((delta_t > 0, np.array([True])))

        self.TLE_list = np.array(TLE_list_original)[mask]
        self.TLE_time_utc = [TLE.epoch.jd for TLE in self.TLE_list]

        return 0

    def propagate(self, t):
        """
        Propagate the satellite's state to a specific time.

        Parameters
        ----------
        t : float
            The Julian date timestamp for the propagation.

        Returns
        -------
        time : astropy.time.Time class
            Final time
        r : ndarray
            Cartesian position vector (x, y, z) [km]
        v : ndarray
            Cartesian velocity vector (vx, vy, vz) [km/s]
        """

        upper_index = np.searchsorted(self.TLE_time_utc, t)
        lower_index = upper_index - 1

        t0 = self.TLE_list[lower_index].epoch
        delta_t = t - self.TLE_list[lower_index].epoch.jd

        time, r, v = _propagate(self.TLE_list[lower_index], t0, delta_t)
        self._test = self.TLE_list[lower_index]

        return time, r, v

    def propagate_array(self, times):
        """
        Propagate the satellite's state for an array of times.

        Parameters
        ----------
        times : ndarray (n)
            The Julian date timestamps for the propagation.

        Returns
        -------
        time_final : ndarray of astropy.time.Time class (n)
            Final times
        r : ndarray (3, n)
            Cartesian position vector (x, y, z) [km]
        v : ndarray (3, n)
            Cartesian velocity vector (vx, vy, vz) [km/s]
        """

        r, v, time_final = [], [], []
        for t in times:
            ti, ri, vi = self.propagate(t)
            r.append(ri)
            v.append(vi)
            time_final.append(ti)

        time_final, r, v = np.array(time_final), np.array(r), np.array(v)

        return time_final, r, v

    def generate_orbital_data(self, dates):
        """
        Calculate and retrieve orbital data (position and velocity) and magnetic
        field data for a given set of times.

        Parameters
        ----------
        dates : list of datetimes
            List with the instants of interest expressed as dates

        Notes
        -----
        This method calculates orbital and magnetic field data for the given
        time points. It transforms the position and velocity coordinates into
        astropy classes, converts them to the International Terrestrial
        Reference System (ITRS), and then calculates spherical coordinates (rho,
        theta, phi). Finally, it computes the magnetic field vectors (B) using
        the IGRF model and returns the data in a Pandas DataFrame.

        The resulting DataFrame is also stored in the object as 'df' for further
        analysis or visualization.
        """

        times = [date.to_julian_date() for date in dates]
        t, r_ECI, v_ECI = self.propagate_array(times)

        # create TEME frame (ECI)
        teme_p = CartesianRepresentation(r_ECI.T * u.km)
        teme_v = CartesianDifferential(v_ECI.T * u.km / u.s)
        teme = TEME(teme_p.with_differentials(teme_v), obstime=t)

        # Transform to ITRS frame (ECEF)
        itrs = teme.transform_to(ITRS(obstime=t))
        x, y, z = itrs.x.value, itrs.y.value, itrs.z.value
        r_ECEF = np.vstack((x, y, z)).T

        # transform to spherical coordinates (ECEF)
        rho, theta, phi = cartesian_to_spherical(x, y, z)
        t_datetime = [date.datetime for date in t]
        Br, Btheta, Bphi = ppigrf.igrf_gc(rho, theta, phi, t_datetime)

        B_sph = np.vstack((Br.diagonal(), Btheta.diagonal(), Bphi.diagonal())).T

        B_ECEF = np.zeros_like(B_sph)

        # transform from spherical frame to ECEF cartesian frame
        for i in range(len(Br.diagonal())):
            B_ECEF[i, :] = Rc2s(theta[i], phi[i]).T @ B_sph[i, :]

        data = np.concatenate((r_ECI, v_ECI, B_ECEF, r_ECEF), axis=1)

        df = pd.DataFrame(
            data,
            columns=[
                "x",
                "y",
                "z",
                "vx",
                "vy",
                "vz",
                "Bx",
                "By",
                "Bz",
                "x_ECEF",
                "y_ECEF",
                "z_ECEF",
            ],
            index=dates,
        )

        self.orbital_df = df

        return 0

    def load_TM(self):
        """
        Loads data for the TM dataframe. GS TM is loaded from the specified
        file, Satnogs TM is retrieved from all files in the folder.
        """

        self.TM_df = curate_TM_data(self.GS_TM_filename)

        return 0

    def generate_csv(self, save=True):
        """
        Generates a CSV file containing combined data from the TM and orbital
        data.

        Parameters
        ----------
        save : bool
            If True, saves csv file. Deafult is True.
        """

        self.load_TM()
        self.generate_orbital_data(self.TM_df.index)

        self.TM_all_df = self.TM_df.join(self.orbital_df)

        if save:
            self.save_data()

        return 0

    def save_data(self):
        """
        Saves the combined dataframe (TM_all_df) to a CSV file.
        """

        if self.folder is None:
            file_path = self.filename + ".csv"
        else:
            os.makedirs(self.folder, exist_ok=True)
            file_path = self.folder + "/" + self.filename + ".csv"

        self.TM_all_df.to_csv(file_path, index=False, sep=";")

        return 0
